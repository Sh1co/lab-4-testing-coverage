package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ForumDAOTests {

    JdbcTemplate mockJdbc;
    ForumDAO forum;
    String threadDate = "16.12.2020";
    String threadSlug = "test_slug";
    int threadLimit = 10;
    UserDAO.UserMapper USER_MAPPER;

    @BeforeEach
    void setupThreadList() {
        mockJdbc = mock(JdbcTemplate.class);
        forum = new ForumDAO(mockJdbc);
        USER_MAPPER = new UserDAO.UserMapper();
    }

    @Test
    @DisplayName("UserList with desc set to false")
    void UserListTest1() {
        ForumDAO.UserList(threadSlug, threadLimit, threadDate, false);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("UserList with desc set to null, since set to null and limit set to null")
    void UserListTest2() {
        ForumDAO.UserList(threadSlug, null, null, null);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("UserList with desc set to true")
    void UserListTest3() {
        ForumDAO.UserList(threadSlug, threadLimit, threadDate, true);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

}
